const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.post('/',(request,response)=>{
    const { movie_title, movie_release_date,movie_time,director_name } = request.body

    const query = `INSERT INTO movie ( movie_title, movie_release_date,movie_time,director_name) VALUES ('${movie_title}',${movie_release_date},'${movie_time}','${director_name}')`

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.get('/',(request,response)=>{
    const { movie_id,movie_title, movie_release_date,movie_time,director_name } = request.body

    const query = `SELECT * FROM movie`

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.put('/:id',(request,response)=>{
    const {movie_id} = request.params
    const { movie_release_date,movie_time} = request.body

    const query = `UPDATE movie SET movie_release_date = ${movie_release_date} and movie_time =  '${movie_time} where movie_id=${movie_id}'`

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.delete('/:id',(request,response)=>{
    const {movie_id} = request.params
   // const { movie_title, movie_release_date,movie_time,director_name } = request.body

    const query = `DELETE FROM movie WHERE movie_id=${movie_id}`

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

module.exports = router

