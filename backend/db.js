const db = require('mysql2')


const pool = db.createPool({
    host:'demodb',
    user: 'root',
    password: 'root',
    database: 'movie-db',
    port: 3306,
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0,
})

module.exports = pool