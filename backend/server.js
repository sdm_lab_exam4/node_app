const express = require('express')
const db = require('mysql2')
const routerMovie= require('./routes/movie')


const app = express()

app.use(express.json())

app.use('/movie', routerMovie)

app.listen(4000,'0.0.0.0', ()=>{
    console.log('server started on port 4000')
})
